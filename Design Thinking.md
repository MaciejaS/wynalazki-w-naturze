# Praca domowa - biomimetyka

Zgodnie z przedstawioną prezentacją "Orbital cleaner" przygotuj własny pomysł na rozwiązanie dowolnego problemu społecznego/ technicznego najlepiej aby koncept inspirowany był naturą.

Opisz rozwiązanie (max 2 str A4). Możesz wykorzystać grafiki, zdjęcia, własne rysunki, stworzyć mapę myśli, wykonanie dowolne, kreatywne. Finalnie opisz w kilku zdaniach koncepcję, tak żebym mogła zrozumieć sens wynalazku.

Termin oddania: **11.12.2023**

## Empatia

### Kryzys Energetyczny

<img src="https://ocdn.eu/pulscms-transforms/1/Wztk9kpTURBXy82NDMxNWVjZWU4ZDg5NGFhZDg5ZGY5OWM4MjYxMDhhNi5qcGeSlQMAEs0WMs0MfJMFzQSwzQJY3gABoTAB" style="width: 350px" />

## Definiowanie

#### _Problem_

- Zanieczyszczanie środowiska "brudną" energią
- Wyczerpywanie się skończonych źródeł energii
- Wysokie zużycie energii przez codzienne przedmioty

#### _Potrzeby_

- "Czysta" / odnawialna energia, nie wykorzystująca nieodnawialnych zasobów
- Mniejsze zapotrzebowanie na energię

#### _Oczekiwania_

- Generowanie energii bez zbędnych odpadów
- Generowanie energii bez zużywania skończonych zasobów
- Redukcja energii używanej przez najbardziej energochłonne urządzenia/systemy

#### _Cel_

- Zredukować używanie brudnej energii
- Zastąpić całą brudną energię, energią odnawialną
- Zredukować potrzeby energetyczne różnych przedmiotów/systemów

## Pomysły

1. **Wykorzystanie fotosyntezy do produkcji energii** - materiały biomimetyczne inspirowane strukturami i funkcjami komórek roślinnych mogą zwiększyć przechwytywanie i konwersję światła słonecznego na energię elektryczną.\
   <img src="https://upload.wikimedia.org/wikipedia/commons/5/50/Fotosynteza3.png" style="width: 150px" />

2. **Magazynowanie energii** - w naturze występują efektywne mechanizmy magazynowania energii, takie jak sposób, w jaki rośliny przechowują energię poprzez procesy chemiczne. Biomimetyka może inspirować rozwój lepszych rozwiązań magazynowania energii, takich jak baterie czy kondensatory oparte na systemach naturalnych, co może prowadzić do bardziej trwałych i pojemniejszych urządzeń magazynowania energii.

3. **Wykorzystywanie efektywnych materiałów i struktur** - wzorowanie się na efektywnym wykorzystaniu materiałów i struktur w naturze może pomóc w projektowaniu lżejszych, silniejszych i bardziej elastycznych materiałów do budowy energooszczędnych budynków, pojazdów lub infrastruktury, zmniejszając zapotrzebowanie na energię w przemyśle i transporcie.

4. **Produkcja biopaliw** - procesy biomimetyczne mogą pomóc w opracowaniu bardziej zrównoważonych i wydajnych metod produkcji biopaliw, naśladując procesy biologiczne występujące w niektórych roślinach lub mikroorganizmach, które naturalnie wytwarzają paliwa.\
   <img src="https://carboncredits.b-cdn.net/wp-content/uploads/2022/08/Biofuel-e1660339778651.jpg" style="width: 250px" />

## Prototyp

### Sztuczne drzewa

1. Pobieranie promieni słonecznych poprzez "liście"\
   <img src="https://cdn.discordapp.com/attachments/902273735640375370/1183832676965298196/image.png?ex=6589c518&is=65775018&hm=b6e1906aabd972f19058604b138be18334e5b367bf7e7983b10dcd45b6aa0699&" style="width: 250px">

2. Zamiana fotonów na energię elektryczną\
   <img src="https://cdn.discordapp.com/attachments/902273735640375370/1183833302138884209/image.png?ex=6589c5ad&is=657750ad&hm=c0b40029257ca13dd5d840c40b840d7db5301f2034aa1e4f1ba0b82ee7dd72a6&" style="width: 250px">

3. Przesył lub przechowywanie energii elektrycznej w "pniu"\
   <img src="https://cdn.discordapp.com/attachments/902273735640375370/1183834489064018040/image.png?ex=6589c6c8&is=657751c8&hm=a9fe8605cad6e1dafed091089f560825a12ec34afa615fecc9f1597a1b5795c7&" style="width: 250px"/>
