# Wynalazki inspirowane naturą 12 - **Omówienie zadań z Design Thinking**

_Omawiamy prace Design Thinking_

<style>
    .row {
        display: flex;
        justify-content: space-between
    }
    .between {
        display: flex;
        flex-direction: column;
    }
    center {
        font-weight: bold;
    }
</style>

<div class="row">
    <div class="between">
        <center>Topniejące lodowce</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193952125953245214/image.png?ex=65ae9591&is=659c2091&hm=4f52edac01ec0e16570ab927098063f6eeafc0694e1ae6f72a66cfd2d11e5f2f&" style="width: 350px">
    </div>
    <div class="between">
        <center>Mikrofabryka w Miejscu Zamieszkania</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193953393950064640/image.png?ex=65ae96bf&is=659c21bf&hm=1a88f74912aed4233272fd533454b2fdcd9067879facbaa578c3fa7315374df2&" style="width: 350px">
    </div>
</div>
<br>
<div class="row">
    <div class="between">
        <center>Transport wody na ogródkach działkowych</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193955385917325422/image.png?ex=65ae989a&is=659c239a&hm=d5b1ed7b88dde3fd5fdd9e04f7fb834bf711481d33fb476df1b6f28ba511d386&" style="width: 350px">
    </div>
    <div class="between">
        <center>Odstraszacz zwierząt leśnych</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193956039347945483/image.png?ex=65ae9936&is=659c2436&hm=47676ba3f2226aad0fad1b2bb99bb942c177aeea23997523859b8c8e32c35210&" style="width: 350px">
    </div>
</div>
<br>
<div>
    <center>Filtracji wody oparta na liściach lotosu</center>
    <div style="display: flex; justify-content: center;">
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193958448606498996/image.png?ex=65ae9b74&is=659c2674&hm=bd8aadd34fcfd860dc4bbb3c6970bb9ecc147b1cebb14ed26429bdc139d5b8a9&" style="width: 340px">
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193959191971377162/image.png?ex=65ae9c26&is=659c2726&hm=6bbdee096aea41720d870bdba06bb1197525368e3fa44d1e89941c9253aa01af&" style="width: 340px">
    </div>
</div>

<!-- Przerwa -->
<p style="height: 150px"></p>

<div class="row">
    <div class="between">
        <center>Uwalnianie leków w organizmie</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193957683326361691/image.png?ex=65ae9abe&is=659c25be&hm=32dcc9e1843916b580623fdca4ccd04a63b41db90587e9ab4a92c1369fc804df&" style="width: 350px">
    </div>
    <div class="between">
        <center>EcoWings</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193959271881261136/image.png?ex=65ae9c39&is=659c2739&hm=59cefdc24de13950175ddc1b8ae1d173b142a5eece4293a8ee33ebcf7696c657&" style="width: 350px">
    </div>
</div>
<br>
<div class="row">
    <div class="between">
        <center>SolarSynth Purifier</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193961760047255572/image.png?ex=65ae9e8a&is=659c298a&hm=b0278417528438bf099924245ef98e0e3c8118990db2bc44580389586af5a43c&" style="width: 350px">
    </div>
    <div class="between">
        <center>Zanieczyszczenie wody</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193962762901135360/image.png?ex=65ae9f79&is=659c2a79&hm=f43db6a17ac14278122f7c39c89ee2df4e74efbe2415278005212b15c85c912e&" style="width: 350px">
    </div>
</div>
<br>
<div class="row">
    <div class="between">
        <center>AquaHarvest</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193965125309321287/image.png?ex=65aea1ac&is=659c2cac&hm=56661240f812a26b0be92393f4caf4d429d629386fc47446828dd0898b250e56&" style="width: 350px">
    </div>
    <div class="between">
        <center>Bardziej wytrzymały asfalt</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193966567118078052/image.png?ex=65aea304&is=659c2e04&hm=9e93aa5715f4ae8f5bccc8882f0d21377e25cacd219f5e02bb8ed54cc687f102&" style="width: 350px">
    </div>
</div>
<br>
<div class="row">
    <div class="between">
        <center>Zanieczyszczenie rzek śmieciami</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193968007756652594/image.png?ex=65aea45b&is=659c2f5b&hm=9ab1d17a279ad39bf01c2a3b5b326c088cb3c4d75d928640ec8095363dcbee29&" style="width: 350px">
    </div>
    <div class="between">
        <center>Korzystaj mądrze</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193971126997618818/image.png?ex=65aea743&is=659c3243&hm=7d1c7552c29ba5d4656b2df28a18890fa08ca7497f18c7b5d800b232d9115020&" style="width: 350px">
    </div>
</div>

<!-- Przerwa -->
<p style="height: 150px"></p>

<div class="row">
    <div class="between">
        <center>Kable bakteryjne</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193972190509551716/image.png?ex=65aea841&is=659c3341&hm=4b4888076392fe84a2f4ff3a3911b2f7aa3225d5058fc431d6e2082c65e6616f&" style="width: 350px">
    </div>
    <div class="between">
        <center>Sztuczne drzewa</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193972646967263333/image.png?ex=65aea8ad&is=659c33ad&hm=b9b6c7334ac47d9b8cad791ec0fb3782e6fafeafa4d63570df85865de1928c1a&" style="width: 350px">
    </div>
</div>
<br>
<div class="row">
    <div class="between">
        <center>RootFlow</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193973259805405294/image.png?ex=65aea940&is=659c3440&hm=fff4f25fc02ac04103c91d49b6bb6677629d28c1a8cafe8ce55e708519a195b0&" style="width: 350px">
    </div>
    <div class="between">
        <center>ClearAir Towers</center>
        <img src="https://cdn.discordapp.com/attachments/902273735640375370/1193973703382421634/image.png?ex=65aea9a9&is=659c34a9&hm=4d2ab13ee98da433a96b2cb87c2502aad39a61476b97f5a14357614cf38bf130&" style="width: 350px">
    </div>
</div>
