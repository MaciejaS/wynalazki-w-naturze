# Wynalazki inspirowane naturą

**Test zaliczeniowy** - _22.01.2024_

**Poprawa** - _05.02.2024_

---

### Tematy wykładów:

**Wykład 1** - \
<img src="https://cdn.discordapp.com/attachments/902273735640375370/1193950414735953920/aa6.png?ex=65ae93f9&is=659c1ef9&hm=f50bdc6bd36a05fbf50b3729faa499e389b9c799050aa033327205b84ee2ea61&" alt="gone">

<!-- style="width: 350px"> -->

**Wykład 2** - _Bionika_

**Wykład 3** - _Biomechanika_

**Wykład 4** - _Biomimetyka_

**Wykład 5** - _Biomateriały_

**Wykład 6** - _Fraktale_

**Wykład 7** - _Wynalazki, patenty_

**Wykład 8** - _Australia (xd?)_

**Wykład 9** - _Patenty_

**Wykład 10** - _Design Thinking/Praca domowa_

**Wykład 11** - _Biomedycyna (zapomniałem przyjść... ups xd)_\
<img src="https://cdn.discordapp.com/attachments/902273735640375370/1193950165409730690/415289212_387883357126000_5014205273430461751_n.jpg?ex=65ae93bd&is=659c1ebd&hm=a4b8cb9c473a4faa3ccc19d60be22d6b38ea6325c28839a446d46b910e27979b&" alt="oops" >

**Wykład 12** - _Omawianie prac Design Thinking_

**Wykład 13** - _Design Thinking p.2 + pytania na egzamin_

**Wykład 14** - _???_

**Wykład 15** - _???_
